# Writing applications in Angular 2 [part 12] #

## Observables and writing Observable services ##

Okay, first of all, what is Observable and what's the buzz around this word. Observable or more precisely [Observable asynchronous pattern](http://reactivex.io/documentation/observable.html) is something which will probably land in ES7 and it's the **pattern for getting the data asynchronously from the back-end server**. A lot of people is used to use **Promises** for this. But with [Promise](https://spring.io/understanding/javascript-promises) you will get the data from server just once, however with Observable service you can get the data repeatedly.

## Observables in Angular 2 ##

Observables are accommodated in Angular 2 using [RxJS](http://reactivex.io/) third party library. So let's show a basic demo of how to use it. For starters, From RxJS you can benefit from the following data types:

* **Observable** is a data type through which you're getting the data from the the Observable source which can be service invoked via Http call or something else.

* **Subject** is an data type for creating the source of observable data. For example in the situation when you're not calling Http service and you're creating your own observable sequence. When returning data via Subject, always offer them **via Observable by converting the Subject to it!**

* **BehaviorSubject** is more or less the same as the Subject, but it offers ability of returning the last value which ran through the Observable service.

## Using of Observables in Angular 2 ##

Let's change the Product service used in my demo into service with Observable API.

```
import { Injectable } from '@angular/core';
import { ProductType } from './product';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductService {
    private _all_product_list = [
                new ProductType('ATI Radeon', 5000, 'http://pctuning.tyden.cz/ilustrace3/Sulc/radeon_hd5870/hd4870.jpg'),
                new ProductType('NVidia GTX 750', 6000, 'http://www.gigabyte.com.au/News/1272/2.jpg'),
                new ProductType('Hal 3000', 25000, 'http://www.hal3000.cz/obrazky/HAL3000_PR/HAL3000%20M%C4%8CR%20Pro(1).jpg'),
                new ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
                ];

    private _best_seller_product_list = [
                new ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
                ]; 

    public loadAllProducts() : Observable<ProductType[]>{        
        return Observable.create(observer => {
                // simulate delay of calling the backend for loading all products.
                setTimeout(() => {
                    observer.next(this._all_product_list);
                }, 2000);
            }
        );
    }

    public loadBestSellerProductList() : Observable<ProductType[]>{
        return Observable.create(observer => {
                // simulate delay of calling the backend for loading best selling products.
                setTimeout(() => {
                    observer.next(this._best_seller_product_list);
                }, 2000);
        });
    }

    public addProduct(product: ProductType) {
        this._all_product_list.push(product);
    }
}
```
I'm simulating here calling of Http service with setTimeout, anyway, methods loadAllProducts and loadBestSellerProductList will return appropriate Product lists if client of service will subscribe to it. So how we can subscribe to it? 
Simply, by calling subscribe method of returned Observable. But there is an better way, when you want to display the returned result then Angular 2 has pipe **directive called async**. Which will safe you from writing the subscribe boilerplate code by subscribing to Observable automatically. Let's show the example:

```
import { Component, OnInit} from '@angular/core';
import { ProductService } from './product.service';
import { ProductType } from './product';
import { ProductDetailComponent } from './product.detail.component';
import { ProductFormComponent } from './product-form.component';
import { provideRouter, RouterConfig } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'products',
    template: `     
              <ul>
                  <li *ngFor="let product of $products | async ">
                     <product-detail [product]="product"></product-detail>
                  </li>
              </ul>       
    `,
    directives: [ProductDetailComponent]
})
export class ProductsComponent implements OnInit {

    public $products : Observable<ProductType[]>;

    constructor(protected _productService: ProductService) {
    }

    public ngOnInit() {
        this.$products = this.loadProducts();
    }

    protected loadProducts() : Observable<ProductType[]>{
        return this._productService.loadAllProducts();
    }
}
```
loadAllProducts now returns Observable, remember? Async pipe will display data once returned from Observable source. You also don't need to write unsubscribe code from disconnecting from Observable source. Async pipe will do it for you.

## Testing the demo ##

* git clone <this repo's URL>
* npm install
* npm start
* visit localhost:3000

   
