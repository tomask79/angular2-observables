"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var products_component_1 = require('./products.component');
var BestSellerProductsComponent = (function (_super) {
    __extends(BestSellerProductsComponent, _super);
    function BestSellerProductsComponent() {
        _super.apply(this, arguments);
    }
    // Override parent method for getting products.
    BestSellerProductsComponent.prototype.loadProducts = function () {
        return this._productService.loadBestSellerProductList();
    };
    return BestSellerProductsComponent;
}(products_component_1.ProductsComponent));
exports.BestSellerProductsComponent = BestSellerProductsComponent;
//# sourceMappingURL=best-seller.products.component.js.map