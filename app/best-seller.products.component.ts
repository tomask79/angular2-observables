import {ProductsComponent} from './products.component';
import {ProductType} from './product';
import {Observable} from 'rxjs/Observable';

export class BestSellerProductsComponent extends ProductsComponent {

    // Override parent method for getting products.
    public loadProducts() : Observable<ProductType[]>{
        return this._productService.loadBestSellerProductList();
    }
}