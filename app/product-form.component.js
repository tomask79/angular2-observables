"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var product_service_1 = require('./product.service');
var router_1 = require('@angular/router');
var ProductFormComponent = (function () {
    function ProductFormComponent(fb, _dataService, _router) {
        this._dataService = _dataService;
        this._router = _router;
        this.myForm = fb.group({
            name: ['', common_1.Validators.required],
            price: ['', common_1.Validators.compose([common_1.Validators.required])],
            image: ['', common_1.Validators.required]
        });
        this.name = this.myForm.controls['name'];
        this.price = this.myForm.controls['price'];
        this.image = this.myForm.controls['image'];
    }
    ProductFormComponent.prototype.onSubmit = function (submitedValue) {
        this._dataService.addProduct(submitedValue);
        this._router.navigateByUrl("productList");
    };
    ProductFormComponent = __decorate([
        core_1.Component({
            selector: 'product-form',
            template: "\n                <form [ngFormModel]=\"myForm\" (ngSubmit)=\"onSubmit(myForm.value)\">\n\n                    <div>Enter name: <input type=\"text\"  [ngFormControl]=\"name\"/></div>\n                    <div *ngIf=\"!myForm.pristine && name.hasError('required')\"><span style=\"color:red\">Please enter the product name!</span></div>  \n\n                    <div>Enter price: <input type=\"text\" [ngFormControl]=\"price\"/></div>\n                    <div *ngIf=\"!myForm.pristine && price.hasError('required')\"><span style=\"color:red\">Please enter the product price!</span></div>  \n                    <div *ngIf=\"price.hasError('lessThenZeroPriceError')\"><span style=\"color:red\">Price cannot be lower then zero!</span></div>\n\n                    <div>Enter image: <input type=\"text\" [ngFormControl]=\"image\"/></div> \n                    <div *ngIf=\"!myForm.pristine && image.hasError('required')\"><span style=\"color:red\">Please enter the product image!</span></div>  \n\n               \t    <div *ngIf=\"myForm.valid\"><button type=\"submit\">CREATE</button></div>\n                </form> \n    ",
            directives: [common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES],
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder, product_service_1.ProductService, router_1.Router])
    ], ProductFormComponent);
    return ProductFormComponent;
}());
exports.ProductFormComponent = ProductFormComponent;
//# sourceMappingURL=product-form.component.js.map