import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ProductType} from './product';
import {CORE_DIRECTIVES, 
        FORM_DIRECTIVES, 
        FormBuilder, 
        ControlGroup, 
        Validators, 
        AbstractControl} from '@angular/common';
import { CustomValidators } from './custom.validations';
import { ProductService } from './product.service';
import { Router } from '@angular/router';

@Component ({
    selector: 'product-form',
    template: `
                <form [ngFormModel]="myForm" (ngSubmit)="onSubmit(myForm.value)">

                    <div>Enter name: <input type="text"  [ngFormControl]="name"/></div>
                    <div *ngIf="!myForm.pristine && name.hasError('required')"><span style="color:red">Please enter the product name!</span></div>  

                    <div>Enter price: <input type="text" [ngFormControl]="price"/></div>
                    <div *ngIf="!myForm.pristine && price.hasError('required')"><span style="color:red">Please enter the product price!</span></div>  
                    <div *ngIf="price.hasError('lessThenZeroPriceError')"><span style="color:red">Price cannot be lower then zero!</span></div>

                    <div>Enter image: <input type="text" [ngFormControl]="image"/></div> 
                    <div *ngIf="!myForm.pristine && image.hasError('required')"><span style="color:red">Please enter the product image!</span></div>  

               	    <div *ngIf="myForm.valid"><button type="submit">CREATE</button></div>
                </form> 
    `,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
})
export class ProductFormComponent {
    myForm: ControlGroup;

    name: AbstractControl;
    price: AbstractControl;
    image: AbstractControl;

    constructor(fb: FormBuilder, private _dataService: ProductService, private _router: Router) {
        this.myForm = fb.group({
            name: ['', Validators.required],
            price: ['', Validators.compose([Validators.required])],
            image: ['', Validators.required]
        });

        this.name = this.myForm.controls['name'];
        this.price = this.myForm.controls['price'];
        this.image = this.myForm.controls['image'];
    }

    onSubmit(submitedValue: ProductType) {
        this._dataService.addProduct(submitedValue);
        this._router.navigateByUrl("productList");
    }
}