"use strict";
var ProductType = (function () {
    function ProductType(name, price, image) {
        this.name = name;
        this.price = price;
        this.image = image;
    }
    return ProductType;
}());
exports.ProductType = ProductType;
//# sourceMappingURL=product.js.map