import { Injectable } from '@angular/core';
import { ProductType } from './product';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductService {
    private _all_product_list = [
                new ProductType('ATI Radeon', 5000, 'http://pctuning.tyden.cz/ilustrace3/Sulc/radeon_hd5870/hd4870.jpg'),
                new ProductType('NVidia GTX 750', 6000, 'http://www.gigabyte.com.au/News/1272/2.jpg'),
                new ProductType('Hal 3000', 25000, 'http://www.hal3000.cz/obrazky/HAL3000_PR/HAL3000%20M%C4%8CR%20Pro(1).jpg'),
                new ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
                ];

    private _best_seller_product_list = [
                new ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
                ]; 

    public loadAllProducts() : Observable<ProductType[]>{        
        return Observable.create(observer => {
                // simulate delay of calling the backend for loading all products.
                setTimeout(() => {
                    observer.next(this._all_product_list);
                }, 2000);
            }
        );
    }

    public loadBestSellerProductList() : Observable<ProductType[]>{
        return Observable.create(observer => {
                // simulate delay of calling the backend for loading best selling products.
                setTimeout(() => {
                    observer.next(this._best_seller_product_list);
                }, 2000);
        });
    }

    public addProduct(product: ProductType) {
        this._all_product_list.push(product);
    }
}