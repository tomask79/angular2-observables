import { Component, OnInit} from '@angular/core';
import { ProductService } from './product.service';
import { ProductType } from './product';
import { ProductDetailComponent } from './product.detail.component';
import { ProductFormComponent } from './product-form.component';
import { provideRouter, RouterConfig } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'products',
    template: `     
              <ul>
                  <li *ngFor="let product of $products | async ">
                     <product-detail [product]="product"></product-detail>
                  </li>
              </ul>       
    `,
    directives: [ProductDetailComponent]
})
export class ProductsComponent implements OnInit {

    public $products : Observable<ProductType[]>;

    constructor(protected _productService: ProductService) {
    }

    public ngOnInit() {
        this.$products = this.loadProducts();
    }

    protected loadProducts() : Observable<ProductType[]>{
        return this._productService.loadAllProducts();
    }
}